﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pharmacy.Models
{
    public class Product
    {
        public Product(string productName, int productCount, double price)
        {
            ProductName = productName;
            ProductCount = productCount;
            Price = price * productCount;
        }

        public Product()
        {
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductId { get; set; }

        public string ProductName { get; set; }
        public int ProductCount { get; set; }
        public double Price { get; set; }
    }

    //public class Product
    //{
    //    public Product(string productName, int productCount, double price, int checkId)
    //    {
    //        ProductName = productName;
    //        ProductCount = productCount;
    //        Price = price * productCount;
    //        CheckId = checkId;
    //    }

    //    public Product()
    //    {
    //    }

    //    //[Key]
    //    //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    //    public int ProductId { get; set; }

    //    public string ProductName { get; set; }
    //    public int ProductCount { get; set; }
    //    public double Price { get; set; }
    //    public int CheckId { get; set; }

    //    [ForeignKey(nameof(CheckId))]
    //    public ProductCheck Check { get; set; }
    //}
}
