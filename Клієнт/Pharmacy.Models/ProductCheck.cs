﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pharmacy.Models
{
    //public class ProductCheck
    //{

    //    public ProductCheck(Pharmacist pharmacist, List<Product> products, double price)
    //    {
    //        PharmacistName = pharmacist.FirstName + " " + pharmacist.LastName;
    //        Products = products;
    //        BuyingDate = DateTime.Now.ToString();
    //        Price = price;
    //    }

    //    public ProductCheck()
    //    {
    //    }

    //    [Key]
    //    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

    //    public string PharmacistName { get; set; }

    //    public string BuyingDate { get; set; }

    //    public double Price { get; set; }

    //    public List<Product> Products { get; set; }
    //}

    public class ProductCheck
    {

        public ProductCheck(Pharmacist pharmacist, double price, string productList)
        {
            PharmacistName = pharmacist.FirstName + " " + pharmacist.LastName;
            BuyingDate = DateTime.Now.ToString();
            Price = price;
            ProductList = productList;
        }

        public ProductCheck()
        {
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CheckId { get; set; }


        public string PharmacistName { get; set; }

        public string BuyingDate { get; set; }

        public double Price { get; set; }

        public string ProductList { get; set; }
    }
}
