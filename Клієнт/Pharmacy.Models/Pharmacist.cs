﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Pharmacy.Models
{
    public class Pharmacist
    {
        public Pharmacist(string firstName, string lastName, string login, string password)
        {
            FirstName = firstName;
            LastName = lastName;
            Login = login;
            Password = password;
        }

        public Pharmacist()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PharmacistId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

    }

}
