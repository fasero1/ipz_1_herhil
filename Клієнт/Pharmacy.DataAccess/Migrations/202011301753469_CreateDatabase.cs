namespace Pharmacy.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pharmacists",
                c => new
                    {
                        PharmacistId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Login = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.PharmacistId);
            
            CreateTable(
                "dbo.ProductChecks",
                c => new
                    {
                        CheckId = c.Int(nullable: false, identity: true),
                        PharmacistName = c.String(),
                        BuyingDate = c.String(),
                        Price = c.Double(nullable: false),
                        ProductList = c.String(),
                    })
                .PrimaryKey(t => t.CheckId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        ProductName = c.String(),
                        ProductCount = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ProductId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Products");
            DropTable("dbo.ProductChecks");
            DropTable("dbo.Pharmacists");
        }
    }
}
