﻿using System.Data.Entity;
using Pharmacy.Models;

namespace Pharmacy.DataAccess.Repository
{
    public class PharmacyContext : DbContext
    {
        public PharmacyContext() : base("server=DESKTOP-5RL1V5D;database=PharmacyDB;trusted_connection=true;")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<PharmacyContext>());
        }

        public DbSet<Pharmacist> Pharmacists { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCheck> ProductChecks { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Product>()
        //        .HasRequired(p => p.Check)
        //        .WithMany(t => t.Products)
        //        .HasForeignKey(p => p.CheckId);
        //}

    }
}
