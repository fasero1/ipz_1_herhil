﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Pharmacy.DataAccess.Repository;
using Pharmacy.Models;

namespace Pharmacy
{
    /// <summary>
    /// Interaction logic for LogIn.xaml
    /// </summary>
    public partial class LogIn : Window
    {
        static int port = 8005; // порт сервера
        static string address = "127.0.0.1"; // адрес сервера



        public void ConnectToSrv(string str)
        {
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(address), port);
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            // подключаемся к удаленному хосту
            socket.Connect(ipPoint);
            string message = str; //Console.ReadLine();
            byte[] data = Encoding.Unicode.GetBytes(message);
            socket.Send(data);
        }




        public LogIn()
        {
            InitializeComponent();
            ConnectToSrv("Connected!");
        }

        private void LogIn_Click(object sender, RoutedEventArgs e)
        {
            if (Login.Text.Length == 0 || Password.Password.Length == 0)
            {
                MessageBox.Show("Fill all fields!");
                ConnectToSrv("Login failed");
            }
            else
            {
                using (PharmacyContext repository = new PharmacyContext())
                {
                    var pharmacists = repository.Pharmacists.ToList();
                    int counter = pharmacists.Count;
                    Pharmacist pharmacist = new Pharmacist();
                    foreach (var p in pharmacists)
                    {
                        if (p.Login != Login.Text || p.Password != Password.Password.ToString())
                        {
                            counter--;
                        }
                        else
                        {
                            pharmacist = p;
                        }
                    }
                    if (counter == 0)
                    {
                        MessageBox.Show("Incorrect login or password!");
                        ConnectToSrv("Login failed");
                    }
                    else
                    {
                        MainWindow main = new MainWindow(pharmacist);
                        main.Show();
                        Close();
                        ConnectToSrv("Login success");
                    }
                }
            }
        }

        private void Reg_Click(object sender, RoutedEventArgs e)
        {
            RegisterWindow reg = new RegisterWindow();
            reg.Show();
            Close();
            ConnectToSrv("Registration");
        }
    }
}
