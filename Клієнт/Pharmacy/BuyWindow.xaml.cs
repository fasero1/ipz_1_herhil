﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using Pharmacy.DataAccess.Repository;
using Pharmacy.Models;

namespace Pharmacy
{   
    /// <summary>
    /// Interaction logic for BuyWindow.xaml
    /// </summary>
    public partial class BuyWindow : Window
    {

        static int port = 8005; // порт сервера
        static string address = "127.0.0.1"; // адрес сервера



        public void ConnectToSrv(string str)
        {
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(address), port);
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            // подключаемся к удаленному хосту
            socket.Connect(ipPoint);
            string message = str; //Console.ReadLine();
            byte[] data = Encoding.Unicode.GetBytes(message);
            socket.Send(data);
        }


        public BuyWindow(Pharmacist pharmacist)
        {
            Pharmacist = pharmacist;
            ProductList = "";
            TempPrice = 0;
            InitializeComponent();
        }

        private Pharmacist Pharmacist { get; set; }
        private string ProductList { get; set; }
        private double TempPrice { get; set; }
        private void Back_Click(object sender, RoutedEventArgs e)
        {

            using (PharmacyContext repository = new PharmacyContext())
            {
                var ProductCheck = new ProductCheck(Pharmacist, TempPrice, ProductList);
                repository.ProductChecks.Add(ProductCheck);
                repository.SaveChanges();
            }

            Close();
            ConnectToSrv("Back");
        }

        private void Buy_Click(object sender,  RoutedEventArgs e)
        {

            if (ProductName.Text.Length == 0 || ProductCount.Text.Length == 0)
            {
                MessageBox.Show("Fill all fields!");
                ConnectToSrv("Operation failed");
            }
            else
            {
                Random rand = new Random();
                Product product = new Product(ProductName.Text, Convert.ToInt32(ProductCount.Text), (rand.Next() % 250 + 1));

                ProductList += "# " + product.ProductName + " " + product.Price / product.ProductCount + "x" +
                    product.ProductCount + "        " + product.Price + "\n";
                TempPrice += product.Price;

                ProductName.Clear();
                ProductCount.Clear();
                MessageBox.Show("Product added to check");
                ConnectToSrv("Operation confirmed.\nProduct added to check");
            }
        }
    }
    //    public BuyWindow(Pharmacist pharmacist)
    //    {
    //        Pharmacist = pharmacist;
    //        Products = new List<Product>();
    //        TempPrice = 0;
    //        InitializeComponent();
    //        ProductCheck = new ProductCheck(Pharmacist, TempPrice);
    //        using (PharmacyContext repository = new PharmacyContext())
    //        {
    //            repository.ProductChecks.Add(ProductCheck);
    //            repository.SaveChanges();
    //        }
    //    }

    //    private Pharmacist Pharmacist { get; set; }
    //    private ProductCheck ProductCheck { get; set; }
    //    private List<Product> Products { get; set; }
    //    private double TempPrice { get; set; }
    //    private void Back_Click(object sender, RoutedEventArgs e)
    //    {
    //        //ProductCheck = new ProductCheck(Pharmacist, TempPrice);

    //        using (PharmacyContext repository = new PharmacyContext())
    //        {
    //            MessageBox.Show(ProductCheck.Products.Count.ToString());

    //            repository.ProductChecks.Last().Price = TempPrice;
    //            repository.SaveChanges();

    //        }

    //        Close();
    //    }

    //    private void Buy_Click(object sender, RoutedEventArgs e)
    //    {

    //        if (ProductName.Text.Length == 0 || ProductCount.Text.Length == 0)
    //        {
    //            MessageBox.Show("Fill all fields!");
    //        }
    //        else
    //        {
    //            using (PharmacyContext repository = new PharmacyContext())
    //            {
    //                Random rand = new Random();
    //                Product product = new Product(ProductName.Text, Convert.ToInt32(ProductCount.Text),
    //                    (rand.Next() % 250 + 1), checkId:repository.ProductChecks.Count() + 1);

    //                repository.Products.Add(product);
    //                repository.SaveChanges();

    //                TempPrice += product.Price;
    //                ProductName.Clear();
    //                ProductCount.Clear();
    //                MessageBox.Show("Product added to check");
    //            }
    //        }
    //    }
    //}
}
