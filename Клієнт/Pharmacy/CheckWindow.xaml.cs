﻿using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using Pharmacy.DataAccess.Repository;

namespace Pharmacy
{
    /// <summary>
    /// Interaction logic for CheckWindow.xaml
    /// </summary>

    public partial class CheckWindow : Window
    {
        static int port = 8005; // порт сервера
        static string address = "127.0.0.1"; // адрес сервера



        public void ConnectToSrv(string str)
        {
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(address), port);
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            // подключаемся к удаленному хосту
            socket.Connect(ipPoint);
            string message = str; //Console.ReadLine();
            byte[] data = Encoding.Unicode.GetBytes(message);
            socket.Send(data);
        }




        public CheckWindow()
        {
            InitializeComponent();
            using (PharmacyContext context = new PharmacyContext())
            {
                var lastCheck = context.ProductChecks.ToList().Last();

                if (lastCheck != null)
                {
                    TicketBlock.Text += "Pharmacy 3\n" + "Check № --- " + lastCheck.CheckId + "\n";
                    TicketBlock.Text += "-------------------------------\n";

                    TicketBlock.Text += lastCheck.ProductList;

                    TicketBlock.Text += "Sum        = " + lastCheck.Price + "\n";
                    TicketBlock.Text += "Pharmacist: " + lastCheck.PharmacistName + "\n Date: " + lastCheck.BuyingDate;
                }
                else
                {
                    MessageBox.Show("Check not formed");
                }
            }
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            Close();
            ConnectToSrv("Back");
        }

    }
}
