﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Pharmacy.DataAccess.Repository;
using Pharmacy.Models;

namespace Pharmacy
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {
        static int port = 8005; // порт сервера
        static string address = "127.0.0.1"; // адрес сервера



        public void ConnectToSrv(string str)
        {
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(address), port);
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            // подключаемся к удаленному хосту
            socket.Connect(ipPoint);
            string message = str; //Console.ReadLine();
            byte[] data = Encoding.Unicode.GetBytes(message);
            socket.Send(data);
        }


        public RegisterWindow()
        {
            InitializeComponent();
        }

        private void Reg_Click(object sender, RoutedEventArgs e)
        {
            if (FirstName.Text.Length == 0 || LastName.Text.Length == 0 || Phone.Text.Length == 0 || Login.Text.Length == 0 || Password.Password.Length == 0)
            {
                MessageBox.Show("Fill all fields!");
                ConnectToSrv("Registration failed");
            }
            else
            {

                using (PharmacyContext repository = new PharmacyContext())
                {
                    Pharmacist pharmacist = new Pharmacist(FirstName.Text, LastName.Text, Login.Text, Password.Password);
                    repository.Pharmacists.Add(pharmacist);

                    repository.SaveChanges();
                    MainWindow main = new MainWindow(pharmacist);
                    main.Show();
                    Close();
                    ConnectToSrv("Registration success");
                }
                Close();
            }
        }
    }
}
