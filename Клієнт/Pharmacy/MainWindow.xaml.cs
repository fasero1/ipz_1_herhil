﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Pharmacy.Models;

namespace Pharmacy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static int port = 8005; // порт сервера
        static string address = "127.0.0.1"; // адрес сервера



        public void ConnectToSrv(string str)
        {
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(address), port);
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            // подключаемся к удаленному хосту
            socket.Connect(ipPoint);
            string message = str; //Console.ReadLine();
            byte[] data = Encoding.Unicode.GetBytes(message);
            socket.Send(data);
        }


        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindow(Pharmacist pharmacist)
        {
            Pharmacist = pharmacist;
            InitializeComponent();
        }

        public Pharmacist Pharmacist { get; set; }

        private void Print_Click(object sender, RoutedEventArgs e)
        {
            CheckWindow checkWindow = new CheckWindow();
            checkWindow.Show();
            ConnectToSrv("Print check");
        }

        private void Buy_Click(object sender, RoutedEventArgs e)
        {
            BuyWindow buyWindow = new BuyWindow(Pharmacist);
            buyWindow.Show();
            ConnectToSrv("Buy products");
        }

        private void Logout_Click(object sender, RoutedEventArgs e)
        {
            LogIn LoginWindow = new LogIn();
            LoginWindow.Show();
            Close();
            ConnectToSrv("Logged out");
        }
    }
}
