﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Server
{
    class Program
    {
        static int port = 8005; // порт
        static void Main(string[] args)
        {
            //адреса для запуску сокета
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);

            Socket listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                //сокет + локальна точка
                listenSocket.Bind(ipPoint);


                listenSocket.Listen(10);

                Console.WriteLine("Servrer started. Waiting for connection...");

                while (true)
                {
                    Socket handler = listenSocket.Accept();
                    // отримуємо повідомлення
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    byte[] data = new byte[256]; // буфер данних

                    do
                    {
                        bytes = handler.Receive(data);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (handler.Available > 0);

                    Console.WriteLine(DateTime.Now.ToShortTimeString() + ": " + builder.ToString());


                    //string message = "повідомлення доставлено";
                    //data = Encoding.Unicode.GetBytes(message);
                    //handler.Send(data);
                    // закриваємо сокет
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
